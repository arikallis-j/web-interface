

**Function of manage.py:**
> *Working with your project*

**Function of __init__.py:**
> *Appending special parameters*

**Function of asgi.py and wsgi:**
> *Correct working with server*

**Function of settings.py:**
> *Setting of your project*

**Function of urls.py:**
> *Monitoring of your apps*

## Creating django-project
> `django-admin startproject project_name`

## Running your server
> `python3 manage.py runserver`

## Creating your app
> `python3 manage.py startapp name_app`

> *name_project/settings.py -> add to INSTALLED_APPS* `'name_app'`

> *name_project/urls.py -> > add to this file code:* 
```python
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls'))
]
```

> *name_app -> add new file urls.py*
> *add to this file code:*
```python 
from django.urls import path
from . import views
urlpatterns = [
    path('', views.index)
]
```

> *name_app -> add new file views.py*
> *add to this file code:*
```python 
from django.http import HttpResponse

def index(request):
    return HttpResponse("YOUR HTML CODE")
```

## Creating HTML-templates

> *name_app -> add templates -> add main*

> *add file index.html*

> *add to views.py code:*
```python 
from django.shortcuts import render

def index(request):
    return render(request, 'main/index.html')
```
