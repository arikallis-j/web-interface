from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def index(request):
    return render(request, 'main/index.html')

def info(request):
    return HttpResponse("<p>Helga's project for Dmitry Zagorulya<p>")