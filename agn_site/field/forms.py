from .models import Jets
from django.forms import ModelForm, CheckboxInput

class JetForm(ModelForm):
    class Meta:
        model = Jets
        fields = ['real_class']
    
    widgets = {
        'real_class': CheckboxInput(attrs={
            'class': 'form-control',
            'placeholder': 'Тип'
        })
    }