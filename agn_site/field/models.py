from django.db import models

# Create your models here.

class Jets(models.Model):
    file_name =  models.CharField(("name"), max_length=50)
    ai_class = models.IntegerField(("ai-class" ))
    real_class = models.IntegerField(("real-class"))

    def __str__(self):
        return self.file_name

    class Meta:
        verbose_name = "Джет"
        verbose_name_plural = "Джеты"
