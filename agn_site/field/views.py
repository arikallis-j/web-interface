from django.shortcuts import render

from .models import Jets
from .forms import JetForm

def field(request):    
    jets = Jets.objects.all()
    form = JetForm
    data = {
        'jets': jets,
        'form': form,
    }
    return render(request, 'field/index.html', data)